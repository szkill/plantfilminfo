package ru.beniocoder.plantfilminfo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.beniocoder.plantfilminfo.domain.genre.compute.ComputingState;
import ru.beniocoder.plantfilminfo.domain.genre.compute.GenreComputingService;

@SpringBootTest
public class PlantFilmInfoApplicationTests {

    @Autowired
    private GenreComputingService genreComputingService;

    @Test
    void contextLoads() {
    }

    @Test
    void getRes() {
        ComputingState computingState = genreComputingService.computeVoteAverage(18);
        System.out.println(computingState.getResult().block());
    }

}
