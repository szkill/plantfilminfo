package ru.beniocoder.plantfilminfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PlantFilmInfoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlantFilmInfoApplication.class, args);
    }

}
