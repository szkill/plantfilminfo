package ru.beniocoder.plantfilminfo.domain.genre.loader;

import reactor.core.publisher.Mono;
import ru.beniocoder.plantfilminfo.domain.genre.Genre;

import java.util.List;

public interface GenreLoader {

    Mono<List<Genre>> loadGenres();

}