package ru.beniocoder.plantfilminfo.domain.genre.compute;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.beniocoder.plantfilminfo.domain.film.loader.FilmLoader;

@Component
@RequiredArgsConstructor
public class AverageVoteComputeFactory {

    private final FilmLoader filmLoader;

    public AverageVoteCompute create(int genreId) {
        return new AverageVoteCompute(genreId, filmLoader);
    }

}
