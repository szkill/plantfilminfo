package ru.beniocoder.plantfilminfo.domain.genre.compute;

import java.util.Optional;

public interface GenreComputingService {

    ComputingState computeVoteAverage(int genreId);

    Optional<ComputingState> getState(int genreId);

}
