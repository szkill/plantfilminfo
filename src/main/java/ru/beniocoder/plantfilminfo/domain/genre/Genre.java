package ru.beniocoder.plantfilminfo.domain.genre;

import lombok.Value;

@Value
public class Genre {

    private int id;
    private String name;

}
