package ru.beniocoder.plantfilminfo.domain.genre.repository;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.beniocoder.plantfilminfo.domain.genre.Genre;
import ru.beniocoder.plantfilminfo.domain.genre.loader.GenreLoader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class GenreRepositoryImpl implements GenreRepository {

    private final Map<Integer, Genre> genreMap = new HashMap<>();
    @Autowired
    GenreLoader genreLoader1;

    @Autowired
    public GenreRepositoryImpl(GenreLoader genreLoader) {
        List<Genre> genres = genreLoader.loadGenres().block();
        Preconditions.checkState(genres != null && !genres.isEmpty(),
                "not any genre found"
        );

        genres.forEach(genre -> genreMap.put(genre.getId(), genre));
    }

    @Override
    public Optional<Genre> findById(int genreId) {
        return Optional.ofNullable(genreMap.get(genreId));
    }

    @Override
    public boolean existsById(int genreId) {
        return genreMap.containsKey(genreId);
    }

    @Override
    public List<Genre> getAllGenres() {
        return genreLoader1.loadGenres().block();
    }

}
