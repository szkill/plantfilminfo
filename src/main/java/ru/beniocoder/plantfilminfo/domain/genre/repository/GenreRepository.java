package ru.beniocoder.plantfilminfo.domain.genre.repository;

import ru.beniocoder.plantfilminfo.domain.genre.Genre;

import java.util.List;
import java.util.Optional;

public interface GenreRepository {

    Optional<Genre> findById(int genreId);

    boolean existsById(int genreId);

    List<Genre> getAllGenres();

}
