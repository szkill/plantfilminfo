package ru.beniocoder.plantfilminfo.domain.film.loader;

import reactor.core.publisher.Mono;
import ru.beniocoder.plantfilminfo.domain.film.Film;
import ru.beniocoder.plantfilminfo.domain.page.PageData;

public interface FilmLoader {

    Mono<PageData<Film>> loadFilms(int page);

}
