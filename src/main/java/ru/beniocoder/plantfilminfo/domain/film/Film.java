package ru.beniocoder.plantfilminfo.domain.film;

import com.fasterxml.jackson.annotation.JsonCreator;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import lombok.Value;

import java.math.BigDecimal;

@Value
public class Film {

    private BigDecimal voteAverage;
    private IntSet genreIds;

    @JsonCreator
    public Film(BigDecimal voteAverage, int[] genreIds) {
        this.voteAverage = voteAverage;
        this.genreIds = new IntOpenHashSet(genreIds);
    }

    public boolean containsGenre(int genreId) {
        return genreIds.contains(genreId);
    }

}
